### Making Product addon Plugin comptable with yith woocommerce gidt card plugin


### follow steps
 
 -  add this filter in themes functions.php , inorder to show the form fields in product page
     ```
     add_filter('wcpa_display_hooks', function ($hooks) {
         global $product;
         if($product && $product->is_type('gift-card')){
             $hooks["fields"] = ["yith_gift_cards_template_before_add_to_cart_button", 50];
         }
        return $hooks;
     });
    ```

 -  replace js file at path  `/yith-woocommerce-gift-cards-premium/assets/js/ywgc-frontend.js`
    with `js_files/ywgc-frontend.js`
    if you want to overide js file, you can follow the next method
    
#### Another method without replacing js file 

 -  add below code in themes functions.php file 
 
 ```


function add_this_script_footer(){ ?>
   <script>
       function updateGiftCardPrice(){
           var amount_buttons = jQuery('button.ywgc-amount-buttons.selected_button,.ywgc-manual-amount.selected_button');
           if( amount_buttons.length && jQuery(".wcpa_form_outer").length){
               var prod_data = jQuery(".wcpa_form_outer").data("product");
               prod_data.wc_product_price = amount_buttons.val();
               jQuery(".wcpa_form_outer").data("product", prod_data);
               jQuery(".wcpa_form_outer").trigger("wcpa_trigger_update");
               jQuery.wcpaInit();
           }
       }
       jQuery('button.ywgc-amount-buttons,.ywgc-manual-amount').on('click change',function(){
           setTimeout(function(){
               updateGiftCardPrice();
           }, 10);

       })
       setTimeout(function(){
           updateGiftCardPrice();
       }, 10);
   </script>

<?php }
add_action('wp_footer', 'add_this_script_footer',999);

```