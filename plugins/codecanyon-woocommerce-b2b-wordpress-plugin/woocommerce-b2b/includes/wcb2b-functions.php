<?php

/**
 * WooCommerce B2B Functions
 *
 * @version 3.0.7
 */

defined( 'ABSPATH' ) || exit;

// Check if WooCommerce B2B Sales Agents is installed and enabled (not pluggable)
function wcb2bsa_is_active() {
    return in_array( 'woocommerce-b2b-sales-agents/woocommerce-b2b-sales-agents.php', get_option( 'active_plugins' ) );
}

// List of WooCommerce pages (not pluggable)
function wcb2b_get_wc_pages() {
    return array(
        get_option( 'woocommerce_shop_page_id' ),
        get_option( 'woocommerce_cart_page_id' ),
        get_option( 'woocommerce_checkout_page_id' ),
        get_option( 'woocommerce_pay_page_id' ),
        get_option( 'woocommerce_thanks_page_id' ),
        get_option( 'woocommerce_edit_address_page_id' ),
        get_option( 'woocommerce_view_order_page_id' ),
        get_option( 'woocommerce_terms_page_id' )
    );
}

// Retrieve all groups
if ( ! function_exists( 'wcb2b_get_groups' ) ) {
    function wcb2b_get_groups() {
        return new WP_Query( array(
            'post_type'     => array( 'wcb2b_group' ),
            'post_status'   => array( 'publish' ),
            'posts_per_page'=> -1,
            'orderby'       => 'title',
            'order'         => 'ASC'
        ) );
    }
}

// Display login message to guest users
if ( ! function_exists( 'wcb2b_login_message' ) ) {
    function wcb2b_login_message() {
        // If messages can be displayed
        if ( apply_filters( 'wcb2b_display_login_message', true ) ) {
            echo '<p class="wcb2b_login_message"><a href="' . apply_filters( 'wcb2b_login_message_url', get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ) . '">' . apply_filters( 'wcb2b_login_message', esc_html__( 'Please, login to see prices and buy', 'woocommerce-b2b' ) ) . '</a><p>';
        }
    }
}

// Validate VAT number by VIES
if ( ! function_exists( 'wcb2b_valid_vies' ) ) {
    function wcb2b_valid_vies( $country, $vat ) {
        if ( empty( $country ) || empty( $vat ) ) { return true; }
        if ( get_option( 'wcb2b_vies_validation' ) !== 'yes' ) { return true; }
        if ( ! extension_loaded( 'soap' ) ) { return true; }

        // Transcoding: WooCommerce => VIES
        $european = apply_filters( 'wcb2b_vies_countries', array(
            'AT' => 'AT', // Austria
            'BE' => 'BE', // Belgium
            'BG' => 'BG', // Bulgaria
            'CY' => 'CY', // Cyprus
            'CZ' => 'CZ', // Czech Republic
            'DE' => 'DE', // Germany
            'DK' => 'DK', // Denmark
            'EE' => 'EE', // Estonia
            'GR' => 'EL', // Greece
            'ES' => 'ES', // Spain
            'FI' => 'FI', // Finland
            'FR' => 'FR', // France 
            'HR' => 'HR', // Croatia
            'HU' => 'HU', // Hungary
            'IE' => 'IE', // Ireland
            'IT' => 'IT', // Italy
            'LT' => 'LT', // Lithuania
            'LU' => 'LU', // Luxembourg
            'LV' => 'LV', // Latvia
            'MT' => 'MT', // Malta
            'NL' => 'NL', // The Netherlands
            'PL' => 'PL', // Poland
            'PT' => 'PT', // Portugal
            'RO' => 'RO', // Romania
            'SE' => 'SE', // Sweden
            'SI' => 'SI', // Slovenia
            'SK' => 'SK'  // Slovakia
        ) );
        if ( ! in_array( $country, array_keys( $european ) ) ) { return true; }

        // Check if VAT has country code inside
        $vat = str_replace( $european[$country], '', $vat );

        $client = new SoapClient( 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl' );

        $params = array(
            'countryCode' => $european[$country],
            'vatNumber' => $vat
        );
        $response = $client->__soapCall( 'checkVat', array( $params ) );

        return ( $response->valid == 1 );
    }
}

// Calculate prices by group
if ( ! function_exists( 'wcb2b_get_group_price' ) ) {
    function wcb2b_get_group_price( $default_price, $object_id, $type, $customer_id = false, $qty = 0 ) {
        // Default guest group, for advanced price management
        $customer_group_id = get_option( 'wcb2b_guest_group' );
        if ( is_user_logged_in() ) {
            if ( ! $customer_id ) {
                $customer_id = get_current_user_id();
            }
            $customer_group_id = get_the_author_meta( 'wcb2b_group', $customer_id );
        }

        // If there is a customer group ID...
        if ( $customer_group_id ) {
            // Check if group is not deleted
            if ( false !== get_post_status( $customer_group_id ) ) {
                // Get price visibility
                $product_group_hide_prices = get_post_meta( $object_id, 'wcb2b_product_group_hide_prices', true );
                if ( is_array( $product_group_hide_prices ) && in_array( $customer_group_id, $product_group_hide_prices ) ) {
                    return '';
                }

                // Get prices rules settings
                $price_rules = get_option( 'wcb2b_price_rules', array() );

                // Fallback on default price
                $price = $default_price;

                if ( in_array( $price_rules, array( 'single', 'both' ) ) ) {
                    // Retrieve dedicated prices configuration by product
                    $product_group_prices = get_post_meta( $object_id, 'wcb2b_product_group_prices', true );

                    if ( isset( $product_group_prices[$customer_group_id] ) && ! empty( $product_group_prices[$customer_group_id] ) ) {
                        // Retrieve product dedicated prices configuration by group
                        $group_regular_price = $product_group_prices[$customer_group_id]['regular_price'];
                        $group_sale_price = $product_group_prices[$customer_group_id]['sale_price'];

                        // Which type of price I need?
                        switch ( $type ) {
                            case 'regular_price' :
                                // If there is a dedicated regular_price, get it
                                if ( $group_regular_price ) {
                                    $price = $group_regular_price;
                                }
                                break;
                            case 'sale_price' :
                                // If there is a dedicated regular_price, get related sale_price
                                if ( $group_regular_price ) {
                                    $price = $group_sale_price;
                                }
                                break;
                            case 'price' :
                                // If there is a dedicated regular_price, get related final price
                                if ( $group_regular_price ) {
                                    $price = min( array_filter( array( $group_regular_price, $group_sale_price ) ) );
                                }
                                break;
                        }
                    }
                }

                if ( in_array( $price_rules, array( 'global', 'both' ) ) ) {
                    // If current price is not empty...
                    if ( ! empty( $price ) ) {
                        // Retrieve group percentage discount
                        $discount = get_post_meta( $customer_group_id, 'wcb2b_group_discount', true );
                        if ( ! empty( $discount ) ) {
                            $discount = wc_format_decimal( $discount );
                            $price = wc_format_decimal( $price );
                            // Apply discount
                            $price = $price - ( $price * $discount / 100 );
                        }
                    }
                }

                // Look for tier prices
                if ( is_admin() || is_cart() || is_checkout() || ! did_action( 'woocommerce_before_main_content' ) ) {
                    $product_group_tier_prices = get_post_meta( $object_id, 'wcb2b_product_group_tier_prices', true );
                    if ( isset( $product_group_tier_prices[$customer_group_id] ) ) {
                        $product_group_tier_price = $product_group_tier_prices[$customer_group_id];

                        if ( is_admin() ) {
                            foreach ( $product_group_tier_price as $tier_quantity => $tier_price ) {
                                if ( $qty >= $tier_quantity ) {
                                    $price = $tier_price;
                                }
                            }
                        } else {
                            foreach ( WC()->cart->get_cart() as $cart_item ) {
                                $cart_product_id = $cart_item['product_id'];
                                if ( $cart_item['variation_id'] ) {
                                    $cart_product_id = $cart_item['variation_id'];
                                }
                                if ( $object_id == $cart_product_id ) {
                                    foreach ( $product_group_tier_price as $tier_quantity => $tier_price ) {
                                        if ( $cart_item['quantity'] >= $tier_quantity ) {
                                            $price = wc_format_decimal( $tier_price );
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Calculated price
				
				

                    foreach ( WC()->cart->get_cart() as $cart_item ) {
                        $cart_product_id = $cart_item['product_id'];
                        if ( $cart_item['variation_id'] ) {
                            $cart_product_id = $cart_item['variation_id'];
                        }
                        if ( $object_id == $cart_product_id ) {
                            if(isset($cart_item['wcpa_price'])){
                             return   $cart_item['wcpa_price'];
                            }
                        }
                    }


				
                return $price;
            }
        }
        return $default_price;
    }
}

// Display tier prices including or excluding taxes
if ( ! function_exists( 'wcb2b_display_tier_price' ) ) {
    function wcb2b_display_tier_price( $price ) {
        return wc_price( wcb2b_adjust_price_tax( $price ) );
    }
}

// Display recommended retail price
if ( ! function_exists( 'wcb2b_display_rrp' ) ) {
    function wcb2b_display_rrp( $price_html, $object ) {
        global $woocommerce_loop;
        if ( is_product() && is_user_logged_in() && empty( $woocommerce_loop['name'] ) ) {
            $group_id = get_the_author_meta( 'wcb2b_group', get_current_user_id() );
            if ( get_option( 'wcb2b_guest_group', 0 ) != $group_id ) {
                if ( in_array( $object->get_type(), array( 'simple', 'variation' ) ) ) {
                    $rrp = wc_price( get_post_meta( $object->get_id(), '_regular_price', true) );

                    ob_start();
                    wc_get_template( 'single-product/rrp-price.php', array(
                        'wcb2b_rrp' => $rrp
                    ), WCB2B_OVERRIDES, WCB2B_ABSPATH . 'templates/' );
                    $rrp_html = ob_get_contents();
                    ob_clean();
                    $price_html .= $rrp_html;
                }
            }
        }
        return $price_html;
    }
}

// Display barcode
if ( ! function_exists( 'wcb2b_display_barcode' ) ) {
    function wcb2b_display_barcode() {
        global $product;

        $barcode = get_post_meta( $product->get_id(), 'wcb2b_barcode', true );
        if ( apply_filters( 'wcb2b_show_barcode', true ) && ! empty( $barcode ) ) {
            ob_start();
            wc_get_template( 'single-product/barcode.php', array(
                'wcb2b_barcode' => $barcode
            ), WCB2B_OVERRIDES, WCB2B_ABSPATH . 'templates/' );
            return ob_get_contents();
            ob_clean();
        }
    }
}

// Get tier prices including or excluding taxes
if ( ! function_exists( 'wcb2b_adjust_price_tax' ) ) {
    function wcb2b_adjust_price_tax( $price ) {
        global $product;

        $fn = 'wc_get_price_excluding_tax';
        if ( 'incl' === get_option( 'woocommerce_tax_display_shop' ) ) {
            $fn = 'wc_get_price_including_tax';
        }

        if ( get_option( 'wcb2b_split_taxes' ) === 'yes' ) {
            $fn = 'wc_get_price_including_tax';
            if ( is_user_logged_in() ) {
                $group_id = get_the_author_meta( 'wcb2b_group', get_current_user_id() );
                if ( $group_id && $group_id !== get_option( 'wcb2b_guest_group', 0 ) ) {
                    $fn = 'wc_get_price_excluding_tax';
                }
            }
        }
        return $fn( $product, array( 'price' => $price ) );
    }
}

// Return unallowed product categories ids, filtered by user (if logged in) or empty
if ( ! function_exists( 'wcb2b_get_unallowed_terms' ) ) {
    function wcb2b_get_unallowed_terms() {
        return ! empty( WC()->wcb2b_unallowed_terms ) ? WC()->wcb2b_unallowed_terms : array();
    }
}

// Return unallowed products ids (belonging unallowed product categories), filtered by user (if logged in) or empty
if ( ! function_exists( 'wcb2b_get_unallowed_products' ) ) {
    function wcb2b_get_unallowed_products() {
        return ! empty( WC()->wcb2b_unallowed_products ) ? WC()->wcb2b_unallowed_products : array();
    }
}

// Set unallowed terms for current customer group in WooCommerce global class
if ( ! function_exists( 'wcb2b_set_unallowed_terms' ) ) {
    function wcb2b_set_unallowed_terms() {
        // Guests default
        $user_group = get_option( 'wcb2b_guest_group' );

        if ( is_user_logged_in() ) {
            $user_group = get_the_author_meta( 'wcb2b_group', get_current_user_id() );

            // Fallback if group not exists
            if ( false === get_post_status( $user_group ) ) { $user_group = get_option( 'wcb2b_guest_group' ); }
        }

        // Empty = All, so we fix with a fake ID
        $unallowed = array();

        // If no terms, return empty array
        if ( $terms = get_terms( array( 'taxonomy' => 'product_cat', 'hide_empty' => false ) ) ) {
            // Check for each term if can be visible
            foreach ( $terms as $term ) {
                if ( ! $group_visibility = get_term_meta( $term->term_id, 'wcb2b_group_visibility', true ) ) {
                    // Fix if term meta is empty
                    $group_visibility = array();
                }
                if ( ! in_array( (int)$user_group, (array)$group_visibility ) ) {
                    $unallowed[] = $term->term_id;
                }
            }
        }
        return $unallowed;
    }
}

// Set unallowed products for current customer group in WooCommerce global class
if ( ! function_exists( 'wcb2b_set_unallowed_products' ) ) {    
    function wcb2b_set_unallowed_products() {
        $args = array(
            'post_type'         => 'product',
            'fields'            => 'ids',
            'posts_per_page'    => -1,
            'tax_query' => array(
                array(
                    'taxonomy'          => 'product_cat',
                    'field'             => 'term_id',
                    'terms'             => wcb2b_get_unallowed_terms(),
                    'operator'          => 'IN',
                    'include_children'  => false
                )
            )
        );
        $products = new WP_Query( $args );
        return $products->post_count ? (array)$products->posts : array( -1 );
    }
}

// Format prices for display
if ( ! function_exists( 'wcb2b_price_format' ) ) {
    function wcb2b_price_format( $price ) {
        $price = number_format(
            floatval( $price ),
            wc_get_price_decimals(),
            wc_get_price_decimal_separator(),
            wc_get_price_thousand_separator()
        );
        if ( apply_filters( 'woocommerce_price_trim_zeros', false ) && wc_get_price_decimals() > 0 ) {
            $price = wc_trim_zeros( $price );
        }
        return $price;
    }
}



// Verify if user has specific roles
if ( ! function_exists( 'wcb2b_has_role' ) ) {
    function wcb2b_has_role( $user_id, $role ) {
        $default = apply_filters( 'wcb2b_has_role_' . $role, array( $role ) );
        $roles = ( new WP_User( $user_id ) )->roles;

        return ! empty( array_intersect( $default, $roles ) );
    }
}

// Get WooCommerce pages to hide with restricted catalog option enabled
if ( ! function_exists( 'wcb2b_get_restricted_pages' ) ) {
    function wcb2b_get_restricted_pages() {
        return apply_filters( 'wcb2b_restricted_pages', wcb2b_get_wc_pages() );
    }
}

// Get always visibile pages
if ( ! function_exists( 'wcb2b_get_always_visible_pages' ) ) {
    function wcb2b_get_always_visible_pages() {
        $pages = wcb2b_get_wc_pages();
        array_push( $pages, get_option( 'page_on_front' ) );
        return apply_filters( 'wcb2b_always_visible_pages', $pages );
    }
}